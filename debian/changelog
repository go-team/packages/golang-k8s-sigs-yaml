golang-k8s-sigs-yaml (1.4.0-2) unstable; urgency=medium

  * d/patches/*: Refresh patch that fixes failed tests on 32 bit arch

 -- Arthur Diniz <arthurbdiniz@gmail.com>  Fri, 15 Nov 2024 17:21:51 +0000

golang-k8s-sigs-yaml (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * d/control:
    - Add Arthur Diniz to Uploaders
    - Declare compliance against Standards-Version 4.7.0
    - Migrate to dh-sequence-golang
    - Remove golang-github-davecgh-go-spew-dev from dependencies
    - Remove golang-gopkg-yaml.v2-dev from dependencies
    - Sync Build-Depends-Indep with Depends stanza
    - Update Build-Depends-Indep and Depends go libraries
  * d/copyright:
    - Add Apache-2.0 related files and copyright authors
    - Add Apache-2.0 short license body
    - Add author Arthur Diniz to debian/* copyright
    - Fix BSD-3-clause license name
    - Remove vendor folder from Files-Excluded
  * d/rules:
    - Install goyaml v2 and v3 NOTICE files via DH_GOLANG_INSTALL_EXTRA
    - Remove --with flag
    - Set --builddirectory flag to _build

 -- Arthur Diniz <arthurbdiniz@gmail.com>  Wed, 04 Sep 2024 22:56:08 +0100

golang-k8s-sigs-yaml (1.3.0-1) unstable; urgency=medium

  * New upstream release v1.3.0
  * Bump debhelper-compat to 13
  * Add Multi-Arch hint
  * Update Standards-Version to 4.6.0 (no changes)

 -- Shengjing Zhu <zhsj@debian.org>  Thu, 24 Feb 2022 23:26:06 +0800

golang-k8s-sigs-yaml (1.2.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Shengjing Zhu ]
  * Backport patch to fix failed tests on arm64
  * Add patch to fix tests on 32 bit arch (Closes: #986672)
  * Update Section to golang
  * Update Standards-Version to 4.5.1 (no changes)

 -- Shengjing Zhu <zhsj@debian.org>  Thu, 15 Apr 2021 23:58:49 +0800

golang-k8s-sigs-yaml (1.2.0-2) unstable; urgency=medium

  * Add patch to only run TestJSONObjectToYAMLObject on amd64

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 08 Mar 2020 04:13:35 +0800

golang-k8s-sigs-yaml (1.2.0-1) unstable; urgency=medium

  * New upstream release
  * Exclude vendor directory in d/copyright
  * Update Standards-Version to 4.5.0 (no changes)
  * Add golang-github-davecgh-go-spew-dev for test depends

 -- Shengjing Zhu <zhsj@debian.org>  Fri, 28 Feb 2020 00:37:23 +0800

golang-k8s-sigs-yaml (1.1.0-2) unstable; urgency=medium

  * Replace golang-yaml.v2-dev with golang-gopkg-yaml.v2-dev

 -- Shengjing Zhu <zhsj@debian.org>  Mon, 20 Jan 2020 11:03:58 +0800

golang-k8s-sigs-yaml (1.1.0-1) unstable; urgency=medium

  * Initial release.

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 05 Jan 2020 18:39:16 +0800
